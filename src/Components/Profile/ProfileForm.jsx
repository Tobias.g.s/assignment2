import { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { deleteTranslations, getSpecificUsers } from "../../Api/utils";
import DisplayValuesBox from "../DisplayValuesBox/DisplayValuesBox";
import "./ProfileForm.css";

export const ProfileForm = () => {

    const [translations, setTranslations] = useState([]);
    const [updateTranslationList, setUpdateTranslationList] = useState(false);
    let userId, userName;

    //Get id and name of logged in user from state
    useSelector((state) => userId = state.user.userId);
    useSelector((state) => userName = state.user.userName);

    //Get translations of the logged in user from API
    useEffect(() => {
        const fetchData = async () => {
            await getSpecificUsers(userName)
            .then(result => {
                setTranslations(result.data[0].translations);
            })
            .catch(err => {
                setTranslations([]);
            })
        }
        fetchData();
    },[updateTranslationList, userName]);

    //Mapping through translations and listitems for each of them
    const showTranslations = translations.map((translation, index) => {
        return <li key={index}>{translation}</li>
    });

    //"Delete" translations from specific user
    const clearAllTranslations = () => {
        deleteTranslations(userId)
        .then(() => {
            setUpdateTranslationList(prevState => !prevState);
        })
    }

    return (
        <div className="profile-page-main-div">
                <DisplayValuesBox style={{height: "70%", width: "30%", marginTop: "5%" }} displayValueBoxFootColor={"#DDDBCB"}>
                    <h1 style={{textAlign: "center"}}>Translations</h1>
                    <ul className="show-translations-list">
                        {showTranslations}
                    </ul>
                    <button onClick={clearAllTranslations} className="profile-page-clear-all-button">
                        Clear all
                    </button>
                </DisplayValuesBox>
        </div>
    )
}