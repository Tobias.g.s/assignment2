import "./Nav.css"
import ProfileMenu from "../MenuButton/ProfileMenu.jsx"
import { useSelector } from "react-redux";


const Nav = () =>{
    let userName = useSelector((state) => state.user.userName);
    if(userName !== ""){
        return(
            <nav>
                <p className="nav-heading">Signify</p>
                <ProfileMenu/>
            </nav>
        );
    }
    return(
        <nav>
            <p className="nav-heading">Signify</p>
        </nav>
    );
}

export default Nav;