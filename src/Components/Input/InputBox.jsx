import "./InputBox.css";

const InputBox = (props) =>{
    // this component takes in input form a text input field and passes the data to state.

    return(
        <>
            <div className="input-box">
                <form className="input-box-text-field" onSubmit={props.data.handleSubmit}>
                    <img className="pen-image"src={process.env.PUBLIC_URL+ "/Images/pen.png"} alt="" />
                    <div className="line"/>
                    <input type="text" className="input-box-text" maxLength={props.data.maxLengthInputBox} id={props.data.id} placeholder={props.data.defaultValue} onChange={props.data.handleChange}/>
                    <button type="submit" className="input-box-btn"><img className="input-box-btn-image" src={process.env.PUBLIC_URL + "/Images/right-arrow.png"} alt="" /></button>
                </form>
                <div className="input-box-footer"></div>
            </div>
        </>
    );
}

export default InputBox;