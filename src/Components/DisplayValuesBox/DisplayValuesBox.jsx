import './DisplayValuesBox.css';

const DisplayValuesBox = ({displayValueBoxFootColor, children, ...props}) => {

    return (
        <div {...props} className='value-box-main-div'>
            {children}
            <div style={{backgroundColor: displayValueBoxFootColor}} className='footer-value-box'></div>
        </div>
    );
}

export default DisplayValuesBox;