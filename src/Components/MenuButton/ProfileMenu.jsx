import { useState } from "react";
import { useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";
import { updateUserId, resetUserName } from "../../State/UserReducer";
import "./ProfileMenu.css";

const ProfileMenu = ()=>{
    const dispatch = useDispatch();
    const [opened, setOpened] = useState(false);
    const navigate = useNavigate();
    const handleOnClick =(event)=>{
        setOpened(!opened)
    }
    const handleProfile =()=>{
        navigate("/profile")
    }
    const handleLogOut =()=>{
        dispatch(resetUserName())
        dispatch(updateUserId(0))
        navigate("/")
    }
    const handleTranslate =()=>{
        navigate("/translate")
    }
    return(
        <>
         {(!opened)
                ? (
                    <div className="profile-menu">
                        <button className="profile-btn" onClick={handleOnClick}>
                            <img src={process.env.PUBLIC_URL + "/Images/profile-svgrepo-com.svg"} className="profile-icon" alt="Profile Icon." />
                        </button>
                    </div>
                )
                : (
                    <div className="profile-menu-open">
                        <button className="profile-btn" onClick={handleOnClick}>
                            <img src={process.env.PUBLIC_URL + "/Images/profile-svgrepo-com.svg"} className="profile-icon" alt="Profile Icon." />
                        </button>
                        <button onClick={handleProfile}>Profile</button>
                        <button onClick={handleTranslate}>Translate</button>
                        <button onClick={handleLogOut}>Logout</button>
                    </div>
                )
            }  
        </>
    );
}
export default ProfileMenu;
