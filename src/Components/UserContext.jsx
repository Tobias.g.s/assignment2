import { createContext, useContext, useState } from "react";

// context
export const UserContext = createContext(null);

// provider
const UserProvider = ({children}) => {
    const [user, setUser] = useState(UserContext);

    return(
        <UserContext.Provider value={[user, setUser]}>
            {children}
        </UserContext.Provider>
    );
}

export default UserProvider;    