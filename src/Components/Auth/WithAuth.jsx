import { useSelector, useDispatch } from "react-redux";
import { Navigate } from "react-router-dom";
import  { getUsers,pushUser } from "../../Api/utils";
import { updateUserId } from "../../State/UserReducer";

// authenticates user before returning requested component.
const withAuth = Component => props =>{
    // gets username from redux userStore.
    let userName = useSelector((state) => state.user.userName);
    const dispatch = useDispatch()
    // checks if user exists in the databse, if yes continue, else it creates a new user with given username and incremented id.
     let validate = async()=>{
        // get users from database
        let response = await getUsers();
        // get the highest id of users.
        let latestId = Math.max([...response.data.map(x => x.id)])
        let currentId = 0;
        // map out usernames
        const userNames = [...response.data.map(x => x.username)];

        // check if user exists in database.
        if (!userNames.includes(userName)){
            // make new user
           await pushUser(userName, latestId+=1);
             currentId = latestId+=1;
           
        }else{
            let users = [...response.data];
            for (const user of users) {
                if (userName === user.username) {
                    currentId = user.id;
                };
            };
        }
        dispatch(updateUserId(currentId));

    };

    // checks if the username is blank.
    if (userName !== "") {     
        validate();
        return <Component {...props} />;
    }
    return <Navigate to="/"/>
}

export default withAuth;