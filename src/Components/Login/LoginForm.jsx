import { useState } from "react";
import { useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";
import { updateUserName } from "../../State/UserReducer";
import InputBox from "../Input/InputBox";
import "./LoginForm.css"

function LoginForm(){
    const [userName, setUsername] = useState({});
    const dispatch = useDispatch()
    let navigate = useNavigate();

    // handler methods passed to InputBox component.
    const handleChange = (event) => {
        // updates username state
        setUsername(event.target.value);
    }

    const handleSubmit = (event) =>{
        // check data is valid
        event.preventDefault();
        dispatch(updateUserName(userName));
        // navigate to translation
        navigate("/translate");
    }

    return(
        <div className="background-mid">
            {
                <>
                    <div className="login-main">
                        
                        <div className="login-main-content">
                            <img className="main-image" src={process.env.PUBLIC_URL + "/Images/sign-language.png"} alt=""/>    
                            <InputBox  data={{defaultValue:"Whats your name?", id:"1", handleChange, handleSubmit}}/>
                        </div>
                    </div>
                </>
            }
        </div>
    );
}
export default LoginForm
