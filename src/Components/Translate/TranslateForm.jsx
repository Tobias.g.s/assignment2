import { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { addTranslation, getSpecificUsers } from "../../Api/utils";
import DisplayValuesBox from "../DisplayValuesBox/DisplayValuesBox";
import InputBox from "../Input/InputBox";
import "./TranslateForm.css";

export const TranslateForm = () => {

    const [translation, setTranslation] = useState("");
    const [existingTranslations, setExistingTranslations] = useState([]);
    const [signImages, setSignImages] = useState([]);
    const [userId, setUserId] = useState();
    let userName;

    //Get id and name of logged in user
    //useSelector((state) => userId = state.user.userId);
    useSelector((state) => userName = state.user.userName);
    

    //Get translations of logged in user from API putting them in an array to later push new translation in and then patch to API
    useEffect(() => {    
        let fetchData = async () => {
            await getSpecificUsers(userName)
            .then(response => {
                if (response.data[0]) {
                    setExistingTranslations(response.data[0].translations);
                    setUserId(response.data[0].id);
                } 
                //A bad hack to get the function to run again when a new user is made, cause the user is not in the API when this runs the first time
                else {
                    fetchData();
                }
            })
        }
        fetchData();
    }, [userName])

    const handleChange = (event) => {
        setTranslation(event.target.value);
    }

    //When button is clicked, push new translation to the array of translations and patch them to the API
    const handleSubmit = (event) =>{
        event.preventDefault();

        //If there are 10 translations, remove the first one and add the new one, else add the new one - To not have more than the last 10 translations
        if (existingTranslations.length === 10) {
            existingTranslations.shift();
            existingTranslations.push(translation);
        } else {
            existingTranslations.push(translation);
        }

        const putData = async () =>  {
            await addTranslation(userId, existingTranslations);
        }

        putData();
        showSigns();
    }

    //Mapping thorugh array of chars from input and using this to display images of the signs
    const showSigns = () => {
        const lowerCaseSigns = translation.toLowerCase();
        let signsArray = lowerCaseSigns.split("");
 
        setSignImages(signsArray.map((signImage, index) => {
            let whatIsDisplayed;
            let reg = new RegExp("^([a-z])$");
        if (reg.test(signImage)) {
                whatIsDisplayed = <img key={index} src={`Images/signs/${signImage}.png`} alt="" className="sign-image"></img>
            } else {
                whatIsDisplayed = <img key={index} src={`Images/signs/space.png`} alt="" className="sign-image"></img>
            }
            return whatIsDisplayed;
        }));
        
    }


    return (
        <div className="translate-page-main-div">
            <div className="div-for-img-and-inputbox">
                <div style={{marginTop: "30px", marginRight: "250px"}}>
                    <img style={{maxWidth: "300px", position: "absolute"}} src={"Images/translateImage.svg"} alt=""></img>
                </div>
                <div style={{marginTop: "160px"}}>
                    <InputBox data={{defaultValue: "Whatchu Thinking?", id: "1", maxLengthInputBox: "32", handleChange, handleSubmit}}/>
                </div>
            </div>
                <DisplayValuesBox style={{display: "flex", height: "480px", width: "850px", marginTop: "1%", justifyContent: "center"}} displayValueBoxFootColor={"#1B9AAA"}>
                    <div style={{marginTop: "20px",width: "95%", height: "88%" }}>
                        {signImages}
                    </div>
                </DisplayValuesBox>
        </div>
    )
}