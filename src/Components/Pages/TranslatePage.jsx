import withAuth from "../Auth/WithAuth";
import { TranslateForm } from "../Translate/TranslateForm";

const TranslatePage = () => {
    return (
        <div>
            <TranslateForm />
        </div>
    )
}

export default withAuth(TranslatePage);