import withAuth from "../Auth/WithAuth";
import { ProfileForm } from "../Profile/ProfileForm";

const ProfilePage = () => {
    return (
        <div>
            <ProfileForm />
        </div>
    )
}

export default withAuth(ProfilePage);