import './App.css';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import LoginPage from './Components/Pages/LoginPage';
import TranslatePage from './Components/Pages/TranslatePage';
import ProfilePage from './Components/Pages/ProfilePage';
import Nav from './Components/Nav/Nav';

function App() {
  return (
    <div className="App">
      <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lemon"></link>
        <BrowserRouter>
            <Nav/>
            <Routes>
                <Route path="/" element={<LoginPage/>}/>
                <Route path="/translate" element={<TranslatePage/>}/>
                <Route path="/profile" element={<ProfilePage/>}/>
            </Routes>
        </BrowserRouter>
    </div>
  );
}

export default App;
