import axios from "axios";

const BASE_API_URL = "https://statuesque-complex-carrot.glitch.me/translations";

//Get specific user from API by name
export async function getSpecificUsers(name) {
    const response = await axios.get(`${BASE_API_URL}?username=${name}`);

    if (response.status ===  404) {
        return response.status;
    } else {
        return response;
    }
}

//Add a new translation to a specific user by id
export async function addTranslation(id, data) {
        axios.defaults.headers['X-API-KEY'] = 'signify';
    	const response = await axios.patch(`${BASE_API_URL}/${id}`, {
                translations: data
            })

        if (response.status === 404 || response.status === 401) {
            return response.status;
        } else {
            return response;
        }
}

//"Delete" all translations from a specific user by id
export async function deleteTranslations(id) {
    axios.defaults.headers['X-API-KEY'] = 'signify';
    const response = await axios.patch(`${BASE_API_URL}/${id}`, {
            translations: []
        })

    if (response.status === 404 || response.status === 401) {
        return response.status;
    } else {
        return response;
    }
}

// get user method, take in no parameters, and returns an object containing all users in the database.
export const getUsers = () => {
    return axios.get(BASE_API_URL);    
}
export default getUsers;


// create user method, takes inn userName(string) id(int). the method posts a user to the api.
export const pushUser =(userName, id)=>{
    axios.defaults.headers['X-API-KEY'] = "signify";
    if (userName !== "") {
        return(axios.post(
            BASE_API_URL,
            {
                id: id,
                username: userName,
                translations: []
            }
        ));
    }
}

// delete user method, takes inn id(int) and deletes user with same id.
export const deleteUser =(id)=>{
    axios.defaults.headers['X-API-KEY'] = "signify";
    return(axios.delete(
        BASE_API_URL + "/" + id
    ));
}