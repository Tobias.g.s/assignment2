import { createSlice } from "@reduxjs/toolkit";

export const userSlice = createSlice({
    name: "user",
    initialState: {
        userName: "",
        userId: 0
    },
    reducers: {
        // sett username
        updateUserName: (state, payload) => {
            if (payload.payload[0] !== undefined) {
                state.userName = payload.payload;
            }
        },
        // reset function for "logging out"
        resetUserName:(state) => {
            state.userName = "";
        }
        ,
        updateUserId: (state, payload) => {
            if (payload.payload !== undefined) {
                state.userId = payload.payload;
            }
        }
    }
});

export const {updateUserName, resetUserName,updateUserId} = userSlice.actions;

export default userSlice.reducer;


// 